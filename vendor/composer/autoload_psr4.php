<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'TADS\\App\\' => array($baseDir . '/app'),
    'Kosan\\Framework\\Module\\' => array($baseDir . '/module'),
    'Carbon_Fields\\' => array($vendorDir . '/htmlburger/carbon-fields/core'),
);
