<?php

/**
 * Plugin Name:       Arista Kloker Private
 * Plugin URI:        https://kanglendir.com
 * Description:       Cloacker Plugin.
 * Version:           1.0.9
 * Author:            Babeh Bril
 * Author URI:        kanglendir.com
 */
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) die();
if( !defined( 'ABSPATH' ) ) die();
if( !defined( 'TADS_DIR' ) ) define('TADS_DIR', __DIR__);
if( !defined( 'TADS_ASSETS' ) ) define('TADS_ASSETS', plugins_url( 'assets', __FILE__));

define('TADS_URL', 'https://kanglendir.com');

if ( file_exists( __DIR__ . '/vendor/autoload.php' ) ) {
  require( __DIR__ . '/vendor/autoload.php' );
}
$core = new \TADS\App\TADS(__FILE__);
$core->init();

$myUpdateChecker = \Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/mralfa/kloker-me-private',
	__FILE__,
	'kloker-me'
);
