<?php
namespace TADS\App;

class Helper
{
    public static function crypt( $string, $secret_iv, $action = 'e' ) {
        $secret_key = get_option('arista_license');
     
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash( 'sha256', $secret_key );
        $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
     
        if( $action == 'e' ) {
            $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
        }
        else if( $action == 'd' ){
            $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
        }
     
        return $output;
    }
    
    public static function isWooCommerceVersionGte( $version ) {

        if ( defined( 'WC_VERSION' ) && WC_VERSION ) {
            return version_compare( WC_VERSION, $version, '>=' );
        } else if ( defined( 'WOOCOMMERCE_VERSION' ) && WOOCOMMERCE_VERSION ) {
            return version_compare( WOOCOMMERCE_VERSION, $version, '>=' );
        } else {
            return false;
        }

    }

    public static function onlyNumber($num){
        return preg_replace( '/[^0-9]/', '', $num );
    }

    public static function getUserAgent()
    {
        return $_SERVER['HTTP_USER_AGENT'];
    }

    public static function getIP()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

	public function get_subdistrict_detail($subdistrict_id) {
		ob_start();
		require SEJOLISA_DIR . 'json/subdistrict.json';
		$json_data = ob_get_contents();
		ob_end_clean();

		$subdistricts        = json_decode($json_data, true);
        $key                 = array_search($subdistrict_id, array_column($subdistricts, 'subdistrict_id'));
        $current_subdistrict = $subdistricts[$key];

		return $current_subdistrict;
	}
}
