<?php
namespace TADS\App;

use Kosan\Framework\Module\Core;

defined('ABSPATH') or die('Direct access not allowed');

class TADS extends Core
{
    function actions()
    {
        (new Admin())->init();
        $lic = get_option('_arista_license');
        $check = $this->check_license($lic);
        $data = json_decode($check);
        if($data->success) {
            new Page();
        }
        add_action('request', [$this, 'check_kloking']);
    }
    
    function check_license($license)
    {
        $url = TADS_URL."/api/license";
        $response = wp_remote_post( $url, array(
            'method' => 'POST',
            'body' => array( 'license' => $license ),
        ));
        
        if ( is_wp_error( $response ) ) {
           $error_message = $response->get_error_message();
           return "Something went wrong: $error_message";
        } else {
           $data = $response['body'];
           return $data;
        }
    }

    function check_kloking($query_vars)
    {
        if(isset($query_vars['page'])){
            $post = get_page_by_path($query_vars['pagename'], OBJECT, 'page');
            if($post != null){
                if(get_post_meta($post->ID, "_arista_kloker", true) == 'yes') {
                    $device =get_post_meta($post->ID, "_arista_kloker_device", true);
                    if($this->kloking($device)){
                        $type = get_post_meta($post->ID, "_arista_kloker_type", true);
                        switch ($type) {
                            case 'own':
                                $post = get_post(get_post_meta($post->ID, "_arista_kloker_page", true));
                                $title = $post->post_title;
                                add_theme_support( 'title-tag' );
                                add_filter( 'pre_get_document_title', function() use ($title) {
                                    return $title;
                                });
                                if($post != null){
                                    $query_vars['name'] = $post->post_name;
                                    $query_vars['post_type'] = $post->post_type;
                                    return $query_vars;
                                }
                                break;
                            case 'scrape':
                                $url = get_post_meta($post->ID, "_arista_kloker_url", true);
                                $options = array(
                                    'http' => array(
                                    'method'=> "GET",
                                    'header'=> "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36\r\n"
                                    )
                                );
                                
                                $context = stream_context_create($options);
                                $html = file_get_contents($url, false, $context);
                                $frs = carbon_get_post_meta($post->ID, 'arista_kloker_fr');
                                foreach ($frs as $fr) {
                                    $html = str_replace($fr['find'], $fr['replace'], $html);
                                }
                                echo $html;exit();
                                break;
                            case 'redirect':
                                $url = get_post_meta($post->ID, "_arista_kloker_url_redirect", true);
                                header("location: {$url}");
                                exit();
                                break;
                        }
                    }
                }
                if(get_post_meta($post->ID, "_arista_copy_lp", true) == 'yes') {
                    $url = get_post_meta($post->ID, "_arista_copy_lp_url", true);
                    $ua = get_post_meta($post->ID, "_arista_copy_lp_ua", true);
                    $referer = get_post_meta($post->ID, "_arista_copy_lp_referer", true);
                    $options = array(
                        'http' => array(
                        'method'=> "GET",
                        'header'=> "User-Agent: {$ua}\r\n".
                            "Referer: {$referer}\r\n"
                        )
                    );
                    
                    $context = stream_context_create($options);
                    $html = file_get_contents($url, false, $context);
                    $frs = carbon_get_post_meta($post->ID, 'arista_copy_lp_fr');
                    foreach ($frs as $fr) {
                        $html = str_replace($fr['find'], $fr['replace'], $html);
                    }
                    echo $html;exit();
                }
                return $query_vars;
            }
        }
        return $query_vars;
    }

    public function kloking($device) {
		$ref = @$_SERVER['HTTP_REFERER'];
        $ua = @$_SERVER['HTTP_USER_AGENT'];
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        
        $url = TADS_URL."/api/cloacker";
        $ip = $this->getUserIP();
        $license = get_option('_arista_license');
        $response = wp_remote_post( $url, array(
            'method' => 'POST',
            'body' => array( 'license' => $license, 'ip' => $ip ),
        ));
        if ( is_wp_error( $response ) ) {
           return false;
        } else {
           $check = $response['body'];
           $data = json_decode($check);
           if(!$data->success && $data->check) {
                switch ($device) {
                    case 'desktop':
                        if($this->isMobile()){
                            return true;
                        }
                        break;
                    case 'mobile':
                        if(!$this->isMobile()){
                            return true;
                        }
                        break;
                    case 'android':
                        if(!$this->isAndroid()){
                            return true;
                        }
                        break;
                    case 'iphone':
                        if(!$this->isIos()){
                            return true;
                        }
                        break;
                }
           }
           return $data->success;
        }
    }
    
    function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", strtolower($_SERVER["HTTP_USER_AGENT"]));
    }

    function isAndroid() {
        return preg_match("/(android|docomo)/i", strtolower($_SERVER["HTTP_USER_AGENT"]));
    }

    function isIos() {
        return preg_match("/(ipod|iphone|ipad)/i", strtolower($_SERVER["HTTP_USER_AGENT"]));
    }

	function getUserIP() {
	    // Get real visitor IP behind CloudFlare network
	    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
	              $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
	              $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
	    }
	    $client  = @$_SERVER['HTTP_CLIENT_IP'];
	    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
	    $remote  = $_SERVER['REMOTE_ADDR'];

	    if(filter_var($client, FILTER_VALIDATE_IP))
	    {
	        $ip = $client;
	    }
	    elseif(filter_var($forward, FILTER_VALIDATE_IP))
	    {
	        $ip = $forward;
	    }
	    else
	    {
	        $ip = $remote;
	    }

	    return $ip;
	}
}
